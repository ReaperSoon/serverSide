package boat;

import java.util.Map;

public abstract class Boat {

	private Integer nbShot = 0;
	protected Integer size;
	private Boolean vertical;
	private Boolean state;
	private int x;
	private int y;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getY() {
		return y;
	}
	public Integer getSize() {

		return size;
	}

	public void setSize(Integer size) {

		this.size = size;
	}

	public Boolean getVertical() {

		return vertical;
	}

	public void setVertical(Boolean vertical) {

		this.vertical = vertical;
	}

	public Boolean getState() {

		return state;
	}

	public void setState(Boolean state) {

		this.state = state;
	}

	public Integer getNbShot() {

		return nbShot;
	}

	public void setNbShot(Integer nbShot) {
		this.nbShot = nbShot;
	}

	public void Draw() {

	}

}