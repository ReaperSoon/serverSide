package boat;




public class Destroyer extends Boat {

	private Integer size;

	public Destroyer() {
		this.size = 1;
	}
	public Integer getSize() {

		return size;
	}

	public void setSize(Integer size) {

		this.size = size;
	}
}