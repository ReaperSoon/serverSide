package boat;




public class Cruiser extends Boat {

	private Integer size;

	public Cruiser() {
		this.size = 2;
	}
	public Integer getSize() {

		return size;
	}

	public void setSize(Integer size) {

		this.size = size;
	}
}