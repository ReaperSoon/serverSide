package boat;




public class Carrier extends Boat {

	private Integer size;

	public Carrier() {
		this.size = 5;
	}
	public Integer getSize() {

		return size;
	}

	public void setSize(Integer size) {

		this.size = size;
	}
}