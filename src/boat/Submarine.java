package boat;




public class Submarine extends Boat {

	private Integer size;

	public Submarine() {
		this.size = 3;
	}
	public Integer getSize() {

		return size;
	}

	public void setSize(Integer size) {

		this.size = size;
	}
}