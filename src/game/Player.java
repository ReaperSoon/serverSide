package game;

import java.net.Socket;
import java.util.ArrayList;

public class Player extends Game {

	private Socket socket;
	private String color;
	private ArrayList listshoot;
	private String ip;
	private GameBoard gameBoard;
	private boolean ready;
	private boolean blocked;

	public boolean isReady() {
		if(gameBoard.isReady()) {
		    return true;
        } else {
		    return false;
        }
	}
	public void setReady(boolean ready) {
		this.ready = ready;
	}
	public GameBoard getGameBoard() {
		return gameBoard;
	}
	public void setGameBoard(GameBoard gameBoard) {
		this.gameBoard = gameBoard;
	}
	public Player(String ip, Socket socket) {
		super();
		this.ip = ip;
		this.blocked = false;
		this.gameBoard = new GameBoard(socket);
	}
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public ArrayList getListshoot() {
		return listshoot;
	}

	public void setListshoot(ArrayList listshoot) {
		this.listshoot = listshoot;
	}

	public String getIp() { return ip; }

	public void setIp(String ip) { this.ip = ip; }
	public boolean isBlocked() {
		return blocked;
	}
	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}
}