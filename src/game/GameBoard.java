package game;


import boat.*;
import network.Chat_ClientServeur;
import sun.security.krb5.internal.crypto.Des;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;


public class GameBoard {

    private Socket socket = null;
    private BufferedReader in = null;
    private PrintWriter out = null;
    private List<Boat> listBoat = null;
    private List<Boat> listBoat2 = null;
    private String[][] board = new String[10][10];
    private Battleship battleship = new Battleship();
    private Carrier carrier = new Carrier();
    private Submarine submarine = new Submarine();
    private Cruiser cruiser = new Cruiser();
    private Destroyer destroyer = new Destroyer();

    public GameBoard(Socket s) {
        this.socket = s;
        this.initBuffers();
        System.out.println("GameBoard constructor");
        this.listBoat = new ArrayList<>();
        this.listBoat2 = new ArrayList<>();
        setBoard();
        printBoard();
    }

    private void initBuffers() {
        try {
            in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            out = new PrintWriter(this.socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Erreur sur la socket !\n");
        }
    }

    public void talk(String msg) {
        out.println(msg);
    }

    public void AddBoatOnBoard(int x, int y, String orientation, String boat) {
        switch (boat) {
            case "BATTLESHIP":
                if (canPlaceShip(x, y, 5)) {
                    battleship.setX(x);
                    battleship.setY(y);
                    if (orientation != "VERTICAL") {
                        battleship.setVertical(false);
                    }
                    addBoat(x, y, orientation, battleship.getSize(), boat);
                    listBoat.add(battleship);
                    listBoat2.add(battleship);
                    talk("vous avez bien ajouté un battleship");
                    printBoard();
                }
                break;
            case "CARRIER":
                if (canPlaceShip(x, y, 4)) {
                    carrier.setX(x);
                    carrier.setY(y);
                    if (orientation != "VERTICAL") {
                        carrier.setVertical(false);
                    }
                    addBoat(x, y, orientation, 4, boat);
                    listBoat.add(carrier);
                    listBoat2.add(carrier);
                    talk("vous avez bien ajouté un carrier");
                    printBoard();
                }
                break;
            case "DESTROYER":
                if (canPlaceShip(x, y, 3)) {
                    destroyer.setX(x);
                    destroyer.setY(y);
                    if (orientation != "VERTICAL") {
                        destroyer.setVertical(false);
                    }
                    addBoat(x, y, orientation, 3, boat);
                    listBoat.add(destroyer);
                    listBoat2.add(destroyer);
                    talk("vous avez bien ajouté un destroyer");
                    printBoard();
                }
                break;
            case "SUBMARINE":
                if (canPlaceShip(x, y, 3)) {
                    submarine.setX(x);
                    submarine.setY(y);
                    if (orientation != "VERTICAL") {
                        submarine.setVertical(false);
                    }
                    addBoat(x, y, orientation, 3, boat);
                    System.out.println("SUBMARINE AJOUTÉ !");
                    listBoat.add(submarine);
                    listBoat2.add(submarine);
                    talk("vous avez bien ajouté un submarine");
                    printBoard();
                    System.out.println("Taille de la liste de bâteaux : " + listBoat.size());
                }
                break;
            case "PATROLBOAT":
                if (canPlaceShip(x, y, 2)) {
                    cruiser.setX(x);
                    cruiser.setY(y);
                    if (orientation != "VERTICAL") {
                        cruiser.setVertical(false);
                    }
                    addBoat(x, y, orientation, 2, boat);
                    talk("vous avez bien ajouté un patrolboat");
                    listBoat.add(cruiser);
                    listBoat2.add(cruiser);
                    printBoard();
                }
                break;
            default:
                talk("Not a ship");
                break;
        }
    }


    public void addBoat(int x, int y, String orientation, int taille, String boat) {
        if (orientation.equals("HORIZONTAL")) {
            for (int i = 0; i <= taille - 1; i++) {
                if (boat.equals("SUBMARINE")) {
                    board[x][y + i] = "s";
                } else if (boat.equals("BATTLESHIP")) {
                    board[x][y + i] = "b";
                } else if (boat.equals("CARRIER")) {
                    board[x][y + i] = "c";

                } else if (boat.equals("DESTROYER")) {
                    board[x][y + i] = "d";

                } else if (boat.equals("PATROLBOAT")) {
                    board[x][y + i] = "p";
                } else {
                    System.out.println("Can't place the boat");
                }

            }
        } else if (orientation.equals("VERTICAL")) {
            for (int i = 0; i <= taille; i++) {
                if (boat.equals("SUBMARINE")) {
                    board[x + i][y] = "s";
                } else if (boat.equals("BATTLESHIP")) {
                    board[x + i][y] = "b";
                } else if (boat.equals("CARRIER")) {
                    board[x + i][y] = "c";
                } else if (boat.equals("DESTROYER")) {
                    board[x + i][y] = "d";
                } else if (boat.equals("PATROLBOAT")) {
                    board[x][y + i] = "p";
                } else {
                    talk("Cant't place the boat");
                }
            }
        } else {
            talk("This orientation does not exist");
        }

    }

    public boolean canPlaceShip(int row, int col, int taille) {
        if (row + taille > 10 || col + taille > 10 || row < 0 || col < 0) {
            talk("Can't place ship here");
            return false;
        } else if (listBoat.size() >= 5) {
            talk("You've already place all your ships ! ");
            return false;
        } else if (board[row][col] != "0") {
            talk("A boat is already places at this place");
            return false;
        } else {
            return true;
        }
    }

    public int fire(int x, int y) {
        if (canFire(x, y)) {
            if (board[x][y] == "s") {
                System.out.println("Touched ! ");
                board[x][y] = "S";
                submarine.setNbShot(submarine.getNbShot() + 1);
                if (submarine.getNbShot() == submarine.getSize()) {
                    System.out.println("Couled");
                    submarine.setState(true);
                    listBoat.remove(submarine);
                    if (listBoat.size() == 0) {
                        talk("Vous avez perdu ! ");
                        return 2;
                    } else {
                        talk("Plus que " + listBoat.size() + "bateau(x)");
                    }
                }
                printBoard();
                return 0;
            } else if (board[x][y] == "b") {
                System.out.println("Touched ! ");
                board[x][y] = "B";
                battleship.setNbShot(battleship.getNbShot() + 1);
                if (battleship.getNbShot() == battleship.getSize()) {
                    System.out.println("Couled");
                    battleship.setState(true);
                    listBoat.remove(battleship);
                    if (listBoat.size() == 0) {
                        talk("Vous avez perdu ! ");
                        return 2;
                    } else {
                        talk("Plus que " + listBoat.size() + "bateau(x)");
                    }
                }

                printBoard();
                return 0;
            } else if (board[x][y] == "c") {
                System.out.println("Touched ! ");
                board[x][y] = "C";
                carrier.setNbShot(carrier.getNbShot() + 1);
                if (carrier.getNbShot() == carrier.getSize()) {
                    System.out.println("Couled");
                    carrier.setState(true);
                    listBoat.remove(carrier);
                    if (listBoat.size() == 0) {
                        talk("Vous avez perdu ! ");
                        return 2;
                    } else {
                        talk("Plus que " + listBoat.size() + "bateau(x)");
                    }
                }
                printBoard();
                return 0;
            } else if (board[x][y] == "p") {
                System.out.println("Touched ! ");
                board[x][y] = "P";
                cruiser.setNbShot(cruiser.getNbShot() + 1);
                if (cruiser.getNbShot() == cruiser.getSize()) {
                    System.out.println("Couled");
                    cruiser.setState(true);
                    listBoat.remove(cruiser);
                    if (listBoat.size() == 0) {
                        talk("Vous avez perdu ! ");
                        return 2;
                    } else {
                        talk("Plus que " + listBoat.size() + "bateau(x)");
                    }
                }
                printBoard();
                return 0;
            } else if (board[x][y] == "d") {
                System.out.println("Touched ! ");
                board[x][y] = "D";
                destroyer.setNbShot(destroyer.getNbShot() + 1);
                if (destroyer.getNbShot() == destroyer.getSize()) {
                    System.out.println("Couled");
                    destroyer.setState(true);
                    listBoat.remove(destroyer);
                    if (listBoat.size() == 0) {
                        talk("Vous avez perdu ! ");
                        return 2;
                    } else {
                        talk("Plus que " + listBoat.size() + "bateau(x)");
                    }
                }
                printBoard();
                return 0;
            } else {
                board[x][y] = "x";
                System.out.println("Rated !");
                printBoard();
                return 1;
            }
        }
        return 1;
    }

    public boolean canFire(int x, int y) {
        if (x > 10 || x < 0 || y > 10 || y < 0) {
            talk("Can't shoot here");
            return false;
        } else if (board[x][y] == "x" || board[x][y] == "B" || board[x][y] == "S" || board[x][y] == "C" || board[x][y] == "P" || board[x][y] == "D") {
            talk("You've already shooted here before !");
            return false;
        } else {
            return true;
        }
    }

    private void setBoard() {
        for (int x = 0; x < board.length; x++) {
            for (int y = 0; y < board[x].length; y++) {
                board[x][y] = "0";
            }
        }
    }

    public boolean isReady() {
        if (listBoat2.size() == 5) {
            talk("Joueur prêt");
            return true;
        }
        return false;
    }

    public void printBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int c = 0; c < board[i].length; c++) {
                out.print(board[i][c] + " ");
            }
            talk(" ");
        }
    }

    public String[][] getBoard() {
        return board;
    }

    public void setBoard(String[][] board) {
        this.board = board;
    }

    public List<Boat> getListBoat() {
        return listBoat;
    }

    public void setListBoat(List<Boat> listBoat) {
        this.listBoat = listBoat;
    }
}