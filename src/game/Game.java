package game;

import network.Chat_ClientServeur;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Game {

    private Socket socket;
    private List<Player> listPlayer = new ArrayList<>();
    Chat_ClientServeur chat;

    public Game(Socket s) {
        socket = s;
        chat = new Chat_ClientServeur(socket);
    }

    public Game() {

    }

    public Player findPlayerByIp(String ip) {
        for (Player player : listPlayer) {
            if (player.getIp() == ip) {
                return player;
            }
        }
        return null;
    }

    public Player findOtherPlayerByIp(String ip) {
        for (Player player : listPlayer) {
            if (player.getIp() != ip) {
                return player;
            }
        }
        return null;
    }


    public void sendLog() {

    }

    public void EndGame(Player player1, Player player2) {
        player1.setBlocked(true);
        player1.getGameBoard().talk("La partie est terminée");
        player2.setBlocked(true);
        player2.getGameBoard().talk("La partie est terminée");
    }

	public void switchPlayer(Player currentPlayer, Player playerToswitch) {
        currentPlayer.setBlocked(true);
        currentPlayer.getGameBoard().talk("stop play please");
        playerToswitch.setBlocked(false);
        playerToswitch.getGameBoard().talk("Vous pouvez jouer.");
	}

	/*public void checkReady(String ipClient) throws IOException {
		if(listPlayer.get(1).isReady() && listPlayer.get(0).isReady()){
			switchPlayer("RED");
		}
	} */

    public void startGame(Player player1, Player player2) {
        player1.setColor("RED");
        getListPlayer().add(player1);
        player2.setColor("BLUE");
        getListPlayer().add(player2);
        GameList.getInstance().addGame(this);
    }

/*	public void fire(String ipClient, String coordx, String coordy) throws IOException {
		Player player =  findOtherPlayerByIp(ipClient);
		PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
		for (Box box : player.getGameBoard().getBoxList()){
			if(box.getCoordX().toString().equals(coordx) && box.getCoordY().toString().equals(coordy)){
				if(box.getShot()){
					try {
						Thread threadMessage = new Thread(new Emission(printWriter, "DEJA TIRER"));
						//Emission emission = new Emission(out, "Bienvenue sur le serveur");
						threadMessage.start();
						if(!threadMessage.isAlive()){
							threadMessage = null;
						}
					} finally {
						switchPlayer(player.getColor());
					}
				}else{
					if(box.getHasBoat()){
						int nbLife = box.getBoat().getNbShot();
						box.getBoat().setNbShot(nbLife + 1);
						if(box.getBoat().getNbShot() == box.getBoat().getSize()){
							try {
								Thread threadMessage = new Thread(new Emission(printWriter, "COULER"));
								//Emission emission = new Emission(out, "Bienvenue sur le serveur");
								threadMessage.start();
								if(!threadMessage.isAlive()){
									threadMessage = null;
								}
							} finally {
								switchPlayer(player.getColor());
							}
						}
					}else {
						try {
							Thread threadMessage = new Thread(new Emission(printWriter, "RATER"));
							//Emission emission = new Emission(out, "Bienvenue sur le serveur");
							threadMessage.start();
							if(!threadMessage.isAlive()){
								threadMessage = null;
							}
						} finally {
							switchPlayer(player.getColor());
						}
					}
				}
			}
		}
} */

    public List<Player> getListPlayer() {
        return listPlayer;
    }

    public void setListPlayer(List<Player> listPlayer) {
        this.listPlayer = listPlayer;
    }
}