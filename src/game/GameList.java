package game;

import java.util.ArrayList;
import java.util.List;

public class GameList {

    private static GameList instance;

    private List<Game> gameList;

    private GameList() {
        this.gameList = new ArrayList<>();
    }

    public static GameList getInstance() {
        if (GameList.instance == null) {
            GameList.instance = new GameList();
        }
        return GameList.instance;
    }

    public void addGame(Game game) {
        gameList.add(game);
    }

    public void deleteGame(Game game) {
        gameList.remove(game);
    }

    public Game findGame(String ip) {
        for (Game game : gameList) {
            List<Player> playerList = game.getListPlayer();
            for (Player player : playerList) {
                if (player.getIp() == ip) {
                    return game;
                }
            }
        }
        return null;
    }
}
