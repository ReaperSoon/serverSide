package network;




import java.io.*;
import java.net.*;


public class Connection_handler implements Runnable{

    private ServerSocket socketserver = null;
    private Socket socket = null;
    private BufferedReader in = null;
    private PrintWriter out = null;

    public Thread t1;
    public Connection_handler(ServerSocket ss){
        socketserver = ss;
    }

    public void run() {

        try {
            while(true){

                socket = socketserver.accept();
                System.out.println("Un joueur vient de se connecter  ");

                t1 = new Thread(new Chat_ClientServeur(socket));
                t1.start();


            }
        } catch (IOException e) {

            System.err.println("Erreur serveur");
        }

    }
}

