package network;

import game.Game;
import game.GameList;
import game.WaitingList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Chat_ClientServeur implements Runnable {

    private Socket socket = null;
    private BufferedReader in = null;
    private PrintWriter out = null;
    private Game game = null;
    private boolean isOver = false;
    private String clientIp;

    public static final String ACT_CHERCHE = "0";
    public static final String ACT_PLACER = "1";
    public static final String ACT_ATTAQUER = "2";
    public static final String ACT_PRET = "3";
    public static final String ACT_FIN_TOUR = "4";
    public static final String ACT_QUIT = "5";

    public Chat_ClientServeur(Socket s) {
        this.socket = s;
        this.clientIp = socket.getRemoteSocketAddress().toString();
        this.initBuffers();
        this.usage("");
    }

    private void initBuffers() {
        try {
            in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
            out = new PrintWriter(this.socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Erreur sur la socket !\n");
        }
    }

    private String[] readCommand() {
        try {
            if (in.ready()) {
                return in.readLine().split(" ");
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Erreur sur la commande !\n");
        }
        return new String[]{};
    }

    public void talk(String msg) {
        out.println(msg);
    }

    private void actionCherche() {
        System.out.println("JOUEUR TROUVÉ : " + this.clientIp);
        int addplayer = WaitingList.getInstance().addPlayer(this.clientIp, socket);
        System.out.println("Valeur de add player : " + addplayer);
        if (addplayer == 0) {
            talk("Démarrage de la partie !");
        } else if (addplayer == 1) {
            talk("En attente d'autres joueurs...");
        } else {
            talk("Vous êtes déjà en recherche de partie");
        }
    }

    private boolean actionPlacer(String[] cmd) {
        if (cmd.length < 5) {
            return false;
        }
        System.out.println("Entrée dans la méthode actionPlacer");
        game = GameList.getInstance().findGame(this.clientIp);
        int x = Integer.parseInt(cmd[1]);
        int y = Integer.parseInt(cmd[2]);
        String orientation = cmd[3];
        String type = cmd[4];
        game.findPlayerByIp(this.clientIp).getGameBoard().AddBoatOnBoard(x, y, orientation, type);
        return true;
    }

    private void actionQuit() {
        try {
            this.isOver = true;
            this.talk("Bye !");
            this.in.close();
            this.out.close();
            this.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean actionFire(String[] cmd) {
        if (cmd.length < 3) {
            System.out.println("Bad arguments !");
            return false;
        }
        System.out.println("Entrée dans la méthode actionFire");
        game = GameList.getInstance().findGame(this.clientIp);
        if (!game.findOtherPlayerByIp(this.clientIp).isReady()) {
            System.out.println("Your enemy isn't ready yet !");
            return false;
        }
        int x = Integer.parseInt(cmd[1]);
        int y = Integer.parseInt(cmd[2]);
        int fire = game.findOtherPlayerByIp(this.clientIp).getGameBoard().fire(x, y);
        if (fire == 0) {
            run();
        } else if (fire == 1){
            game.switchPlayer(game.findPlayerByIp(this.clientIp), game.findOtherPlayerByIp(this.clientIp));
        } else {
            game.EndGame(game.findPlayerByIp(this.clientIp), game.findOtherPlayerByIp(this.clientIp));
        }
        return true;
    }

    private boolean actionReady() {
        talk("Vous etes pret");
        return true;
    }

    private void usage(String action) {
        String usage = "";
        if ("".equals(action)) {
            usage += "Commandes disponibles : \n";
            usage += "\t 0 --> cherche une partie\n";
            usage += "\t 1 --> place un bateau\n";
            usage += "\t 2 --> attaquer\n";
            usage += "\t 3 --> prêt\n";
            usage += "\t 4 --> fin tour\n";
            usage += "\t 5 --> quitter\n";
        } else if (ACT_PLACER.equals(action)) {
            usage += "Paramètres de placement : \n";
            usage += "\t x --> position sur le plateau\n";
            usage += "\t y --> position sur le plateau\n";
            usage += "\t taille --> taille du bateau\n";
            usage += "\t pos --> HORIZONTAL | VERTICAL\n";
            usage += "\t type --> [NOM_DU_BATEAU_SA_MERE]\n";
        } else if (ACT_ATTAQUER.equals(action)) {
            usage += "Paramètres d'attaque : \n";
            usage += "\t x --> position sur le plateau\n";
            usage += "\t y --> position sur le plateau\n";
        }
        this.talk(usage);
    }

    public void run() {
        while (!isOver) {
            String[] cmd = this.readCommand();
            if (cmd.length > 0) {
                String action = cmd[0];
                if (Chat_ClientServeur.ACT_QUIT.equals(action)) {
                    this.actionQuit();
                } else if (Chat_ClientServeur.ACT_ATTAQUER.equals(action)) {
                    if (game.findPlayerByIp(this.clientIp).isBlocked()) {
                        this.talk("ce n'est pas votre tour, vous êtes bloqué");
                    } else {
                        this.actionFire(cmd);
                    }
                } else if (Chat_ClientServeur.ACT_CHERCHE.equals(action)) {
                    this.actionCherche();
                } else if (Chat_ClientServeur.ACT_FIN_TOUR.equals(action)) {
                    this.talk("Not yet implemented !");
                } else if (Chat_ClientServeur.ACT_PLACER.equals(action)) {
                    if (!this.actionPlacer(cmd)) {
                        usage(ACT_PLACER);
                    }
                } else if (Chat_ClientServeur.ACT_PRET.equals(action)) {
                    this.actionReady();
                } else {
                    this.talk("Bad action : " + action);
                    this.usage("");
                }
              /*  case "PRET":
                    System.out.println("Joueur : " + this.clientIp + " est pret");
                    game.checkReady(this.clientIp);
                    break;
                case "FINTOUR":
                    game.switchPlayer(message[1]);
                    System.out.println("Fin du tour");
                    break; */
            }

        }
        System.out.println("Client " + clientIp + " just left !");
    }
}

