package network;

import java.io.*;
import java.net.*;

public class Server {
	public static ServerSocket ss = null;
	public static Thread t;


	public void server() {

		try {
			ss = new ServerSocket(6402);
			System.out.println("Le serveur est à l'écoute du port "+ss.getLocalPort());

			t = new Thread(new Connection_handler(ss));
			t.start();

		} catch (IOException e) {
			System.err.println("Le port "+ss.getLocalPort()+" est déjà utilisé !");
		}

	}


}
